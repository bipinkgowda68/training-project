class Car {
	String brand = "Mercedes";
	String model = "AMG";
	double price = 5000000;
	int seatingCapacity = 2;
	String color = "yellow";
	String regNo = "02-4-M";

	void start() {
		System.out.println(" car Started");
	}

	public static void main(String[] args) {
		Car c1 = new Car();
		Car c2 = new Car();
		Car c3 = new Car();
		System.out.println(c1.color);

		System.out.println(c2.price);
		c3.color = "Red";
		System.out.println(c3.color);
		c1.regNo = "osabb";
		System.out.println(c1.regNo);
		c3.start();
	}

}
