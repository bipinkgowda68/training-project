class Shark {

	String color = "Blue";
	int eyes = 10;
	int fins = 15;
	int tails = 5;

	void swim() {
		System.out.println("Shark is swimming");

	}

	public static void main(String[] args) {
		Shark s1 = new Shark();
		Shark s2 = new Shark();
		Shark s3 = new Shark();
		Shark s4 = new Shark();
		Shark s5 = new Shark();

		System.out.println(s1.color);
		System.out.println(s2.color);
		s1.color = "red";
		s2.color = "pink";
		s3.color = "violet";
		s4.color = "green";
		s5.color = "brown";

		System.out.println(s1.color);
		System.out.println(s2.color);
		System.out.println(s3.color);
		System.out.println(s4.color);
		System.out.println(s5.color);

		s1.swim();
		s2.swim();
		s3.swim();
		s4.swim();
		s5.swim();
	}

}
