package com.bops.scanner;

import java.util.Scanner;

public class Employee {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Employee ID:");
		int ID = s.nextInt();
		System.out.println("Enter employee name:");
		String name = s.nextLine();
		s.nextLine();
		System.out.println("Enter employee number:");
		String num = s.nextLine();
		System.out.println("Enter employee age: ");
		int age = s.nextInt();
		s.nextLine();
		System.out.println("Enter employee mail:");
		String mail = s.nextLine();

		s.close();

		System.out.println("Employee ID:" + ID);
		System.out.println("Employee name:" + name);
		System.out.println("Employee number:" + num);
		System.out.println("Employee age:" + age);
		System.out.println("Employee mail:" + mail);

	}
}
