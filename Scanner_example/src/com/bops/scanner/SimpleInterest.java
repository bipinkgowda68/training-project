package com.bops.scanner;

public class SimpleInterest {
	static double SimpleInterest(int p, double t, double r) {
		return p * t * r / 100;

	}

	public static void main(String[] args) {
		double result = SimpleInterest(100000, 1, 5.5);
		System.out.println(result);
	}
}
