package com.bops.scanner;

import java.util.Scanner;

public class Learning {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter book name:");
		String book = s.nextLine();
		System.out.println("Book number");
		int bookNum = s.nextInt();
		s.close();
		System.out.println("Book name:" + book);
		System.out.println("Book number:" + bookNum);
	}
}
