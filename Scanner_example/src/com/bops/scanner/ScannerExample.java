package com.bops.scanner;

import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter name");
		String name = s.nextLine();
		System.out.println("Enter weight");
		double weight = s.nextDouble();
		s.close();
		System.out.println("DETAILS ARE");
		System.out.println("Name:" + name);
		System.out.println("Weight:" + weight);
	}

}
