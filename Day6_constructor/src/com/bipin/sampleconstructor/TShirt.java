package com.bipin.sampleconstructor;

public class TShirt {
	String color;
	int size;
	String brand;

	public TShirt(String color, int size, String brand) {
		this.color = color;
		this.size = size;
		this.brand = brand;
	}

	public TShirt(String color, int size) {
		this.color = color;
		this.size = size;
	}

	public TShirt(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "TShirt [" + (color != null ? "color=" + color + ", " : "") + "size=" + size + ", "
				+ (brand != null ? "brand=" + brand : "") + "]";
	}

	public static void main(String[] args) {
		TShirt t1 = new TShirt("Red");
		System.out.println(t1);
		TShirt t2 = new TShirt("White", 28);
		System.out.println(t2);
		TShirt t3 = new TShirt("Yellow", 30, "Polo");
		System.out.println(t3);
	}
}
