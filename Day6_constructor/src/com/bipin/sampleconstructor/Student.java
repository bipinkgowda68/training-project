package com.bipin.sampleconstructor;

public class Student {
	String name;
	int age;
	int rollNo;
	int avgPercentage;

	// Alt + sift + S +O(alphabet O)
	// 3 parameter custom constructor
	public Student(int rollNo, String name, int age) {
		this.rollNo = rollNo;
		this.name = name;
		this.age = age;
	}

	// Alt + Sft + S+ S
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", " + (name != null ? "name=" + name + ", " : "") + "age=" + age
				+ ", avgPercentage=" + avgPercentage + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Alpha", 25);
		System.out.println(s1);
	}

}