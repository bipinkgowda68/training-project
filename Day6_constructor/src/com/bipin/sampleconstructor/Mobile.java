package com.bipin.sampleconstructor;

public class Mobile {
	String color;
	int storageCapacity;

	// Alt+ shift + S+ S
	// Instead of address we need to print the content of the object ,so we are
	// overriding toString()
	@Override
	public String toString() {
		return "Mobile [" + (color != null ? "color=" + color + ", " : "") + "storageCapacity=" + storageCapacity + "]";
	}

	public static void main(String[] args) {
		Mobile m1 = new Mobile();
		System.out.println(m1);
	}

}
