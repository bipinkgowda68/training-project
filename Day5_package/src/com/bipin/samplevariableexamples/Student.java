package com.bipin.samplevariableexamples;

public class Student {
	String name;
	int age;
	String collegeName = "xyzit";

	public static void main(String[] args) {
		Student s1 = new Student();
		System.out.println(s1.name);
		System.out.println(s1.age);
		System.out.println(s1.collegeName);
		s1.name = "alpha";
		s1.age = 20;
		System.out.println();

		Student s2 = new Student();
		s2.name = "Bravo";
		s2.age = 21;
		s2.collegeName = "Oxford";
		System.out.println();

	}
}
