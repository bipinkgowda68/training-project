package com.bipin.constructorexamples;

public class Mobile {
	String brand;
	String color;
	double price;

	// Constructor
	// Alt + shift+ s + o
	Mobile(String brand, String color, double price) {
		this.brand = brand;
		this.color = color;
		this.price = price;
	}

	// main method
	public static void main(String[] args) {
		Mobile m1 = new Mobile("Samsung", "Green", 20000.0);
		m1.color = "black";
		System.out.println(m1.color);
	}
}
