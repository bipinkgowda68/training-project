package com.bipin.constructorexamples;

public class Book {
	String bookId;
	String name;
	double price;
	String lang;

	Book(String bookId, String name, double price, String lang) {
		this.bookId = bookId;
		this.name = name;
		this.price = price;
		this.lang = lang;
	}

	public static void main(String[] args) {
		Book b1 = new Book("N1", "Love", 600.0, "English");
		Book b2 = new Book("N2", "Freedom", 200.0, "Kannada");

		System.out.println(b1.name);
		b1.bookId = "n5";
		System.out.println(b1.bookId);
		b2.lang = "hindi";
		System.out.println(b2.lang);

	}

}
