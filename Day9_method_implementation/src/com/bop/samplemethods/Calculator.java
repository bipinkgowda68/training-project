package com.bop.samplemethods;

public class Calculator {
	static int add(int x, int y) {
		int sum = x + y;
		return sum;
	}

	static void printProduct(int x, int y) {
		System.out.println(x * y);
	}

	public static void main(String[] args) {
		System.out.println(Calculator.add(3, 5));
		printProduct(5, 2);
		printProduct(7, 8);
	}
}