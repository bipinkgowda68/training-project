package com.rollsroyce.carwebapplication;

public class Car {
	String brand = "Rolls Royce";
	String color;
	char variant;
	double mileage;

	public Car() {
		this.color = "Black";
		this.variant = 'D';
		this.mileage = 5.0;
	}

	public Car(String color) {
		this.color = color;
		this.variant = 'P';
		this.mileage = 5.5;
	}

	public Car(String color, char variant, double mileage) {
		this.color = color;
		this.variant = variant;
		this.mileage = mileage;
	}

	public static void main(String[] args) {

		Car c1 = new Car();
		System.out.println(c1);
		Car c2 = new Car("Red");
		System.out.println(c2);
		Car c3 = new Car("Blue", 'P', 5.8);
		System.out.println(c3);

	}

	@Override
	public String toString() {
		return "Car [" + (brand != null ? "brand=" + brand + ", " : "") + (color != null ? "color=" + color + ", " : "")
				+ "variant=" + variant + ", mileage=" + mileage + "]";
	}
}
