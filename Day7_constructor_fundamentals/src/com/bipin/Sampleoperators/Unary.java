package com.bipin.Sampleoperators;

public class Unary {
	public static void main(String[] args) {
		int x = 0;
		int y = ++x + x++ + ++x;
		int z = ++y + y++ + x++;
		int a = --y + --z;
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(a);
	}
}
